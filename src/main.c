#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>

#define HEAP 2048
#define MALLOC 111
#define FIRST 222
#define SECOND 333
#define THIRD 444
#define FOURTH 1555
#define MAX 4096


void test1() {
    printf("----TEST 1 (Regular allocation)-----\n");
    void *mem = _malloc(MALLOC);
    debug_heap(stdout, HEAP_START);
    _free(mem);
    printf("----TEST 1 PASSED-----\n");
}

void test2() {
    printf("----TEST 2 (Freeing multiple blocks)-----\n");
    void *mem1 = _malloc(FIRST);
    void *mem2 = _malloc(SECOND);
    void *mem3 = _malloc(THIRD);
    debug_heap(stdout, HEAP_START);
    _free(mem1);
    _free(mem2);
    _free(mem3);
    printf("----TEST 2 PASSED-----\n");
}

void test3() {
    printf("----TEST 3-----\n");
    void *mem1 = _malloc(SECOND);
    void *mem2 = _malloc(THIRD);
    void *mem3 = _malloc(FOURTH);
    _free(mem2);
    debug_heap(stdout, HEAP_START);
    _free(mem1);
    _free(mem3);
    printf("----TEST 3 PASSED-----\n");
}

void test4() {
    printf("----TEST 4-----\n");
    void *mem = _malloc(MAX);
    debug_heap(stdout, HEAP_START);
    _free(mem);
    printf("----TEST 4 PASSED-----\n");
}

void test5() {
    printf("----TEST 5-----\n");
    void *mem1 = _malloc(MAX);
    void *mem2 = _malloc(MAX);
    debug_heap(stdout, HEAP_START);
    _free(mem1);
    _free(mem2);
    printf("----TEST 5 PASSED-----\n");    
}

int main(){
    heap_init(HEAP);
    test1();
    test2();
    test3();
    test4();
    test5();
    return 0;
}
